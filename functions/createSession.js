const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const MongoClient = require('mongodb').MongoClient;
const uri = `mongodb+srv://aws_lambda:${process.env.MONGO_ATLAS_PASSWORD}@rosabianca-dtxke.mongodb.net/test?retryWrites=true&w=majority `;
const client = new MongoClient(uri, { useNewUrlParser: true });
let db;

module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false; //optimize connection
  const productsCheckout = [];
  const { products, email, phone_number, fullName } = JSON.parse(event.body);
  console.log(uri, client)

  // check products
  if (!products || !products.length) {
    const response = {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      body: JSON.stringify({
        error: "products required but not present"
      }),
    };
    return callback(null, response);
  }
  // check email
  if (!email) {
    const response = {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      body: JSON.stringify({
        error: "email requried but not present"
      }),
    };
    return callback(null, response);
  }

  // check fullName
  if (!fullName) {
    const response = {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      body: JSON.stringify({
        error: "fullname requried but not present"
      }),
    };
    return callback(null, response);
  }

  // create productCheckout object with all order products
  products.forEach(product => {
    switch (product.id) {
      case '5e51575815543648b540c69c':
        product = {
          name: "Mazzo 1",
          description: "Un splendido mazzo di mimose",
          amount: 1990, // convert to cents
          quantity: product.qty,
          currency: 'eur',
        };
        break;
      case '5e515776feeb103b5f806fb1':
        product = {
          name: "Mazzo 2",
          description: "Un splendido mazzo di mimose",
          amount: 1990, // convert to cents
          quantity: product.qty,
          currency: 'eur',
        };
        break;
      case '5e515784cacd1b1cb695389a':
        product = {
          name: "Mazzo 3",
          description: "Un splendido mazzo di mimose",
          amount: 1990, // convert to cents
          quantity: product.qty,
          currency: 'eur',
        };
        break;
      case '5o514794cazd1b1eb695389a':
        product = {
          name: "Mazzo 3",
          description: "Un splendido mazzo di mimose",
          amount: 51, // convert to cents
          quantity: product.qty,
          currency: 'eur',
        };
        break;
      default:
        const response = {
          statusCode: 400,
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
          body: JSON.stringify({
            error: "product not found"
          }),
        };
        return callback(null, response);
    }
    productsCheckout.push(product)
  })

  return stripe.checkout.sessions.create({ // Create Stripe session
    payment_method_types: ['card'],
    billing_address_collection: event.body.is_billing_address_required ? 'required' : 'auto', // or auto, collect address data
    customer_email: email,
    // customer: "aws-sub_1%6&1$", //have to be registered on accoutn creation
    line_items: productsCheckout,
    success_url: 'http://localhost:3000/success?session_id={CHECKOUT_SESSION_ID}',
    cancel_url: 'http://localhost:3000/cancel',
    locale: "it"
  })
    .then(async (session) => {
      if (!client.isConnected()) {
        await client.connect()
        db = client.db("rosabianca")
      }
      const ordersCollection = db.collection("orders");
      const trxCollection = db.collection("transactions");

      // add to each product additional fields
      const orders = productsCheckout.map(product => {
        product["stripe_session_id"] = session.id
        product["stripe_payment_intent"] = session.payment_intent,
          product["customer_fullname"] = fullName
        product["customer_phone_number"] = phone_number
        product["status"] = "PENDING"
        product["created_at"] = new Date().toISOString(),
          delete product.currency
        delete product.description
        return product
      })

      // create orders
      const new_orders = await ordersCollection.insertMany(orders)

      // create transaction
      const trx = {
        "created_by": fullName,
        "customer_phone_number": phone_number,
        "status": "PENDING",
        "created_at": new Date().toISOString(),
        "stripe_sesion_id": session.id,
        "customer_email": email,
        "stripe_payment_intent": session.payment_intent
      }
      const new_trx = await trxCollection.insertOne(trx)


      /** RESPONSE **/
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
          message: 'Charge processed succesfully!',
          session,
        }),
      };
      callback(null, response);


    })
    .catch((err) => { // Error response
      console.log(err);
      const response = {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
          error: err.message,
        }),
      };
      callback(null, response);
    });
};
