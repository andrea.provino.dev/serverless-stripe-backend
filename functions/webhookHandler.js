const MongoClient = require('mongodb').MongoClient;
const uri = `mongodb+srv://aws_lambda:${process.env.MONGO_ATLAS_PASSWORD}@rosabianca-dtxke.mongodb.net/test?retryWrites=true&w=majority `;

const client = new MongoClient(uri, { useNewUrlParser: true });
let db;

module.exports.handler = async (event, context, callback) => {
    if (event.body.type === 'charge.succeeded') {
        console.log("Charge Succeded")
        if (!client.isConnected()) {
            await client.connect()
            db = client.db("rosabianca")
            console.log("new db connection")
        }

        const data = event.body.data.object
        console.log("HookCharge")

        const ordersCollection = db.collection("orders");
        const trxCollection = db.collection("transactions");

        await ordersCollection.updateMany({ "stripe_payment_intent": data.payment_intent }, { $set: { "status": "PAYED" } })
        await trxCollection.updateOne({ "stripe_payment_intent": data.payment_intent }, { $set: { "status": "PAYED" } })
        console.log("Updated collection")
        /** RESPONSE */
        const response = {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                message: 'ok'
            }),
        };
        callback(null, response);


    }
}
