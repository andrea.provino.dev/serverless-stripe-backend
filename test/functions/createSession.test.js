const LambdaTester = require('lambda-tester');
const nock = require('nock');
const { session1 } = require('../../test/functions/mock/session');
const createSession = require('../../functions/createSession');


const testEvent = {
  body: session1,
};

beforeAll(() => nock.disableNetConnect());
afterAll(() => nock.enableNetConnect());

describe('createSession', () => {
  describe('success cases', () => {
    beforeEach(() => {
      nock('https://api.stripe.com/v1')
        .post('/checkout/session')
        .reply(200, { success: true });
    });

    test('returns response', () => LambdaTester(createSession.handler)
      .event(testEvent)
      .expectResult((data) => {
        expect(data).toMatchSnapshot();
      }));
  });

  describe('error cases', () => {
    beforeEach(() => {
      nock('https://api.stripe.com/v1')
        .post('/checkout/session')
        .reply(500);
    });

    test('returns response', () => LambdaTester(createSession.handler)
      .event(testEvent)
      .expectResult((data) => {
        expect(data).toMatchSnapshot();
      }));
  });
});
