const session1 = {
  productQty: 1,
  productId: '5e51575815543648b540c69c',
};


const session2 = {
  productQty: 2,
  productId: '5e515776feeb103b5f806fb1',
};

const session3 = {
  productQty: 3,
  productId: '5e515784cacd1b1cb695389a',
};

export {
  session1,
  session2,
  session3,
};
